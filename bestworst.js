(function($) {

  Drupal.behaviors.bwPicker = {
    attach: function(context, settings) {
      $("#best, #worst, #choices").sortable({
        connectWith: ".sortable"
      }).disableSelection();

      $("#bw-sort").click(function() {
        var chosen = $('#best').sortable("toArray");
        console.log(chosen);
        for(choice in chosen) {
          // Got all the way down to the text value, we'll need to build up a json
          // object of the string to pass back to a voting mechanism.
          // We may be able to forgo this loop.
          console.log($('#' + chosen[choice]).text());
        }
        return;
      });
    }
  };

})(jQuery);
